               .model small
.code
org 100h
start:
   jmp mulai
nama   db 13,10,'Nama : $'
hp     db 13,10,'Jenis HP : $'       
tampak_nama db 30,?,30 dup (?)
tampak_hp   db 13,?,13 dup (?)
tampak_kode db 13,?,13 dup (?)

a db 01
b db 02
c db 03
d db 04
e db 05
f db 06
g db 07
h db 08
i db 09
j dw 15

daftar db 13,10,'+++++++++++++++++++++++++++++++++++++++++++++'
       db 13,10,'           DAFTAR PAKET SPOTIFY              '
       db 13,10,'+++++++++++++++++++++++++++++++++++++++++++++'
       db 13,10,'NO|     Paket    |   Tahun |       Harga    |'
       db 13,10,'1.| 1 HARI       |   2021  |   Rp. 2.000    |'
       db 13,10,'2.| 3 HARI       |   2021  |   Rp. 5.000    |'
       db 13,10,'3.| 1 MINGGU     |   2021  |   Rp. 12.000   |'
       db 13,10,'4.| 1 BULAN      |   2021  |   Rp. 27.000   |'
       db 13,10,'5.| 1 TAHUN      |   2021  |   Rp. 200.000  |' 
       db 13,10,'*********************************************'
       db 13,10,'            PILIH PAKET YANG DIPILIH         '
       db 13,10,'********************************************$'
      
error   db 13,10,'ANDA SALAH MEMASUKKAN KODE $'
pilih_ps db 13,10,'Silahkan masukkan tipe paket yang anda pilih $'
succes db 13,10,'SELAMAT $'

   mulai:
   mov ah,09h
   lea dx,nama
   int 21h
   mov ah,0ah
   lea dx,tampak_nama
   int 21h
   push dx
   
   mov ah,09h
   lea dx,hp
   int 21h
   mov ah,0ah
   lea dx,tampak_hp
   int 21h
   push dx
   
   mov ah,09h
   mov dx,offset daftar
   int 21h
   mov ah,09h
   mov ah,01h

   mov ah,09h 
   mov ah,01h
   jmp proses
   jne error_msg

error_msg:
   mov ah,09h
   mov dx,offset error
   int 21h
   int 20h

proses:
  mov ah,09h
  mov dx,offset pilih_ps
  int 21h
  
  mov ah,1
  int 21h
  mov bh,al
  mov ah,1
  int 21h
  mov bl,al
  cmp bh,'0'
  cmp bl,'1'
  je hasil1
  
  cmp bh,'0'
  cmp bl,'2'
  je hasil2
  
  cmp bh,'0'
  cmp bl,'3'
  je hasil3
  
  cmp bh,'0'
  cmp bl,'4'
  je hasil4
  
  cmp bh,'0'
  cmp bl,'5'
  je hasil5
  
  jne error_msg
  
;================================
hasil1:
  mov ah,09h
  lea dx,teks1
  int 21h
  int 20h
  
hasil2:
  mov ah,09h
  lea dx,teks2
  int 21h
  int 20h     
  
hasil3:
  mov ah,09h
  lea dx,teks3
  int 21h
  int 20h     
  
hasil4:
  mov ah,09h
  lea dx,teks4
  int 21h
  int 20h
  
hasil5:
  mov ah,09h
  lea dx,teks5
  int 21h
  int 20h
  
  
;================================='
teks1 db 13,10,'Kamu memilih PAKET 1'
      db 13,10,'Pada tahun 2021'
      db 13,10,'Total Harga sewa yang harus dibayar : Rp. 2.000'
      db 13,10,'Terima Kasih $'
      
teks2 db 13,10,'Kamu memilih PAKET 2'
      db 13,10,'Pada tahun 2021'
      db 13,10,'Total Harga sewa yang harus dibayar : Rp. 5.000'
      db 13,10,'Terima Kasih $'
      
teks3 db 13,10,'Kamu memilih PAKET 3'
      db 13,10,'Pada tahun 2021'
      db 13,10,'Total Harga sewa yang harus dibayar : Rp. 12.000'
      db 13,10,'Terima Kasih $'                           
      
teks4 db 13,10,'Kamu memilih PAKET 4'
      db 13,10,'Pada tahun 2021'
      db 13,10,'Total Harga sewa yang harus dibayar : Rp. 27.000'
      db 13,10,'Terima Kasih $'                             
      
teks5 db 13,10,'Kamu memilih PAKET 5'
      db 13,10,'Pada tahun 2021'
      db 13,10,'Total Harga sewa yang harus dibayar : Rp. 200.000'
      db 13,10,'Terima Kasih $'    
      
end start